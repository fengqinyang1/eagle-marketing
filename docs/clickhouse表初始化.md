- 启动clickhouse服务
```shell
# 启动服务
[root@hdp03 ~]# systemctl start clickhouse-server

# 查看状态
[root@hdp03 ~]# systemctl status clickhouse-server

#客户端连接
[root@hdp03 ~]# clickhouse-client -m
```

```sql
-- 创建用户行为日志明细表

drop table if exists  default.eagle_detail;
create table default.eagle_detail
(
    account           String   ,
    appId             String   ,
    appVersion        String   ,
    carrier           String   ,
    deviceId          String   ,
    deviceType        String   ,
    eventId           String   ,
    ip                String   ,
    latitude          Float64  ,
    longitude         Float64  ,
    netType           String   ,
    osName            String   ,
    osVersion         String   ,
    properties        Map(String,String),
    releaseChannel    String,
    resolution        String,
    sessionId         String,
    timeStamp         Int64 ,
    INDEX u (deviceId) TYPE minmax GRANULARITY 3,
    INDEX t (timeStamp) TYPE minmax GRANULARITY 3
) ENGINE = MergeTree()
ORDER BY (deviceId,timeStamp)
;

-- 创建kafka引擎表(指定Kafka为数据源)
drop table default.eagle_detail_kafka;
create table default.eagle_detail_kafka
(
    account           String   ,
    appId             String   ,
    appVersion        String   ,
    carrier           String   ,
    deviceId          String   ,
    deviceType        String   ,
    eventId           String   ,
    ip                String   ,
    latitude          Float64  ,
    longitude         Float64  ,
    netType           String   ,
    osName            String   ,
    osVersion         String   ,
    properties        Map(String,String),
    releaseChannel    String,
    resolution        String,
    sessionId         String,
    timeStamp         Int64
) ENGINE = Kafka('node-1.51doit.cn:9092,node-2.51doit.cn:9092,node-3.51doit.cn:9092','eagle-applog','group001','JSONEachRow')
;


-- 创建物化视图（桥接kafka引擎表和事件明细表）
drop view eagle_view;
create MATERIALIZED VIEW eagle_view TO eagle_detail
as
select
    account        ,
    appId          ,
    appVersion     ,
    carrier        ,
    deviceId       ,
    deviceType     ,
    eventId        ,
    ip             ,
    latitude       ,
    longitude      ,
    netType        ,
    osName         ,
    osVersion      ,
    properties     ,
    releaseChannel  ,
    resolution      ,
    sessionId       ,
    timeStamp
from eagle_detail_kafka
;

```