create table tb_user (
  `name` varchar(20),
  `score` double
) UNIQUE KEY(`name`)
DISTRIBUTED BY HASH(`name`) BUCKETS 2
PROPERTIES(
  "replication_num" = "1"
);











create table eagle_detail (
  `deviceId`       STRING, 
  `timeStamp`      BIGINT,  
  `id`             STRING,          
  `account`        STRING,      
  `appId`          STRING,        
  `appVersion`     STRING,   
  `carrier`        STRING,        
  `eventId`        STRING,      
  `ip`             STRING,           
  `latitude`       DOUBLE,      
  `longitude`      DOUBLE,     
  `netType`        STRING,      
  `osName`         STRING,       
  `osVersion`      STRING,    
  `releaseChannel` STRING,
  `resolution`     STRING,   
  `sessionId`      STRING,        
  `properties`     STRING
) UNIQUE KEY(`deviceId`, `timeStamp`, `id`)
DISTRIBUTED BY HASH(`deviceId`) BUCKETS 4
PROPERTIES(
  "replication_num" = "1"
);



SELECT get_json_string(`properties`, "$.p1") as v1 from eagle_detail limit 10;

