insert into tb_marketing_rules (rule_name, rule_condition_json, rule_controller_drl, rule_status) values (
'rule0001',
'
{
  "ruleId": "rule_001",
  "triggerEvent": {
    "eventId": "A",
    "properties": {
      "p2": "v1",
      "p20": "v2"
    }
  },
  "profileCondition": {
    "tag2": "v1",
    "tag20": "v1"
  },
  "combineConditions": [
    {
      "cacheId": "001",
      "eventConditions": [
        {
          "eventId": "C",
          "properties": {
            "p12": "v5",
            "p6": "v8"
          }
        }
      ],
      "regex": "(1)",
      "maxLimit": 999,
      "minLimit": 3,
      "sql": "select eventId from eagle_detail where deviceId = ? and timeStamp > ? and timeStamp <= ? order by timeStamp asc",
      "startTime": 1636387200000,
      "endTime": 1637078399000
    },
    {
      "cacheId": "002",
      "eventConditions": [
        {
          "eventId": "E"
        },
        {
          "eventId": "H"
        },
        {
          "eventId": "I"
        }
      ],
      "regex": "(.*?1.*?2.*?3)",
      "maxLimit": 999,
      "minLimit": 1,
      "sql": "select eventId from eagle_detail where deviceId = ? and timeStamp > ? and timeStamp <= ? order by timeStamp asc",
      "startTime": 1636387200000,
      "endTime": 1637078399000
    }
  ]
  
}
',

'
dialect  "java"

import cn.doitedu.demos.Event

rule "first-demo1"
    when
       $e: Event(type == "view" && count == 2)
    then
       System.out.println("8888");
       $e.setHit(true);
end
',
1
);