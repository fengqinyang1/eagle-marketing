package cn.doitedu.cache;

import cn.doitedu.utils.ConnectionUtils;
import org.apache.flink.api.java.utils.ParameterTool;
import redis.clients.jedis.Jedis;

import java.util.Set;

public class CacheManagerRedisImpl implements CacheManager {

    private Jedis jedis;

    public CacheManagerRedisImpl(ParameterTool parameterTool) {
        jedis = ConnectionUtils.getJedis(parameterTool);
    }

    @Override
    public String getData(String bigKey, String smallKey) {
        return jedis.hget(bigKey, smallKey);
    }

    @Override
    public void setValue(String bigKey, String smallKey, String value) {
        jedis.hset(bigKey, smallKey, value);
    }

    @Override
    public void setValueEx(String bigKey, String smallKey, String value, long ttl) {
        jedis.hset(bigKey, smallKey, value);
        jedis.expire(bigKey, 3600*12);
    }

    @Override
    public Set<String> getTimeRanges(String bigKey) {
        return jedis.hkeys(bigKey);
    }
}
