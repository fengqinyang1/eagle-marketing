package cn.doitedu.jobs;


import cn.doitedu.pojo.LogBean;
import cn.doitedu.pojo.MatchResult;
import cn.doitedu.pojo.RulesBean;
import cn.doitedu.udfs.JsonToBeanFunction;
import cn.doitedu.udfs.RulesJsonToBeanFunction;
import cn.doitedu.udfs.RulesMatchFunctionV2;
import cn.doitedu.udfs.RulesMatchFunctionV3;
import cn.doitedu.utils.FlinkUtils;
import cn.doitedu.utils.StateDescriptorUtils;
import com.ververica.cdc.connectors.mysql.source.MySqlSource;
import com.ververica.cdc.debezium.JsonDebeziumDeserializationSchema;
import org.apache.flink.api.common.eventtime.SerializableTimestampAssigner;
import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.serialization.SimpleStringSchema;
import org.apache.flink.streaming.api.datastream.*;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

import java.time.Duration;

/**
 * 根据用户的实时事件匹配动态规则
 */
public class MarketRulesMatch {

    public static void main(String[] args) throws Exception {

        StreamExecutionEnvironment env = FlinkUtils.env;

        MySqlSource<String> mySqlSource = MySqlSource.<String>builder()
                .hostname("node-3.51doit.cn")
                .port(3306)
                .databaseList("doit24") // set captured database
                .tableList("doit24.tb_marketing_rules") // set captured table
                .username("root")
                .password("123456")
                .deserializer(new JsonDebeziumDeserializationSchema()) // converts SourceRecord to JSON String
                .build();


        DataStreamSource<String> rulesStream = env.fromSource(mySqlSource, WatermarkStrategy.noWatermarks(), "MySQL Source")
                .setParallelism(1);

        SingleOutputStreamOperator<RulesBean> rulesBeanStream = rulesStream.process(new RulesJsonToBeanFunction());

        //将规则数据广播出去
        BroadcastStream<RulesBean> broadcastStream = rulesBeanStream.broadcast(StateDescriptorUtils.rulesStateDescriptor);

        /////////////////////

        //从Kafka中读取用户行为数据
        DataStream<String> kafkaStream = FlinkUtils.createKafkaStream(args[0], SimpleStringSchema.class);

        //数据转换清洗
        SingleOutputStreamOperator<LogBean> beanStream = kafkaStream.process(new JsonToBeanFunction());

        SingleOutputStreamOperator<LogBean> logBeanWithWaterMark = beanStream.assignTimestampsAndWatermarks(WatermarkStrategy.<LogBean>forBoundedOutOfOrderness(Duration.ofSeconds(0)).withTimestampAssigner(new SerializableTimestampAssigner<LogBean>() {
            @Override
            public long extractTimestamp(LogBean element, long recordTimestamp) {
                return element.getTimeStamp();
            }
        }));

        //先要根据用户的设备ID进行keyBy
        KeyedStream<LogBean, String> keyedStream = logBeanWithWaterMark.keyBy(LogBean::getDeviceId);

        //将keyBy后的数据（key使用keyedState），与广播的数据进行connect（使用广播状态）
        SingleOutputStreamOperator<MatchResult> matchResult = keyedStream.connect(broadcastStream).process(new RulesMatchFunctionV3());

        matchResult.print();

        env.execute();


    }

}
