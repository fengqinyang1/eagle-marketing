package cn.doitedu.udfs;

import cn.doitedu.pojo.LogBean;
import com.alibaba.fastjson.JSON;
import lombok.extern.log4j.Log4j;
import org.apache.flink.streaming.api.functions.ProcessFunction;
import org.apache.flink.util.Collector;


public class JsonToBeanFunction extends ProcessFunction<String, LogBean> {


    @Override
    public void processElement(String json, Context ctx, Collector<LogBean> out) throws Exception {

        try {
            LogBean bean = JSON.parseObject(json, LogBean.class);
            out.collect(bean);
        } catch (Exception e) {
            //e.printStackTrace();
            //log.error(json);
        }
    }
}
