package cn.doitedu.dao;

import cn.doitedu.pojo.CombineCondition;
import cn.doitedu.pojo.LogBean;
import org.apache.flink.api.java.utils.ParameterTool;

import java.util.List;

public interface HistoryEventDao {

    default void init(ParameterTool parameterTool) throws Exception {};

    default void close() throws Exception {};

    //不论是从状态查询，还是从ClickHouse中查询，还是从未来要切换的DorisDB中查询
    //都需要传入查询条件（LogBean bean, List<CombineCondition> combineConditions），
    //只要返回符合条件的行为字符串即可
    String queryEventSequenceStr(LogBean bean, CombineCondition combineCondition) throws Exception;



}
