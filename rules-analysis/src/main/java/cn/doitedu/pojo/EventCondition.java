package cn.doitedu.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

/**
 * 对EventParam的替换
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class EventCondition {

    //行为
    private String eventId;

    //行为属性
    private Map<String, String> properties;

}
