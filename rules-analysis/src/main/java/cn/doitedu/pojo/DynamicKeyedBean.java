package cn.doitedu.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DynamicKeyedBean {

    private String keyName;

    private String keyValue;

    private LogBean logBean;
}
