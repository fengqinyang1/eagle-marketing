package cn.doitedu.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MatchResult {

    private String deviceId;

    private long eventTime;

    private long triggerTime;

    private Long ruleId;


}
