package cn.doitedu.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 *
 * 组合条件，即将行为次数、行为序列，进行组合
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CombineCondition {

    //sql
    private String sql;

    //时间段
    private long startTime;

    private long endTime;

    //正则表达式
    private String regex;

    //阈值
    private Integer minLimit;

    private Integer maxLimit;

    //事件ID和事件属性(一个或多个)
    private List<EventCondition> eventConditions;

    //cacheId
    private String cacheId;
}
