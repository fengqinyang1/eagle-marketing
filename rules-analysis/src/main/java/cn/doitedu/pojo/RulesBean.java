package cn.doitedu.pojo;

import lombok.Data;

import java.util.Date;

@Data
public class RulesBean {


    private Long id;

    private String rule_name;

    private String rule_condition_json;

    private String rule_controller_drl;

    private int rule_status;

    private Date create_time;

    private Date modify_time;

    private String author;

}
