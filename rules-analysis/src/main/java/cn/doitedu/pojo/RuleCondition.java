package cn.doitedu.pojo;

import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class RuleCondition {

    private Long id;

    //key指定1到多个keyBy的值，用,分割
    private String keyBy;

    //触发的行为规则
    private EventCondition triggerEvent;

    //用户的画像规则
    private Map<String, String> profileCondition;

    //配置的行为次数条件或行为序列条件
    //1.行为次数条件：B事件，做过2次
    //2.行为序列条件：ABD，做过1次
    private List<CombineCondition> combineConditions;


    //使用是基于时间触发的规则
    private boolean hasTimer;

    //时间定时器触发的规则
    private TimerCondition timerCondition;


}
