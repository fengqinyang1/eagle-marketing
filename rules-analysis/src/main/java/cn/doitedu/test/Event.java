package cn.doitedu.test;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * facts 即 数据（封装数据的bean）
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Event {

    private String type;

    private int count;

    private boolean hit;
}
