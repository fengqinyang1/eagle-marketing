package cn.doitedu.test;

import java.lang.reflect.Field;

public class ReflectDemo {


    public static void main(String[] args) throws Exception{

        Event event = new Event("t1", 3, false);

        Class<Event> clazz = Event.class;
        Field field = clazz.getDeclaredField("type");
        field.setAccessible(true);
        String r = (String) field.get(event);
        System.out.println(r);
    }
}
