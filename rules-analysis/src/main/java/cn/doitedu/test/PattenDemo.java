package cn.doitedu.test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PattenDemo {


    public static void main(String[] args) {

        String events = "ACBDFCDABB";
        //判断B出现的次数
//        String regStr = "(B)";
//        Pattern pattern = Pattern.compile(regStr);
//        Matcher matcher = pattern.matcher(events);


        //判断AB行为序列的次数
        String regStr = "(.*?A.*?B)";
        Pattern pattern = Pattern.compile(regStr);
        Matcher matcher = pattern.matcher(events);

        int count = 0;
        while (matcher.find()){
            count++;
        }
        System.out.println(count);

    }

}
