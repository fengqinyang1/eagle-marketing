package cn.doitedu.test;

import org.apache.commons.io.FileUtils;
import org.kie.api.io.ResourceType;
import org.kie.api.runtime.KieSession;
import org.kie.internal.utils.KieHelper;

import java.io.File;

/**
 * drool引擎的入门程序
 */
public class DroolsDemo1 {

    public static void main(String[] args) throws Exception{


        String ruleStr =  FileUtils.readFileToString(new File("my-rules/second-demo.drl"), "utf-8");

        // System.out.println(ruleStr);

        //创建KieSession的工具类
        KieHelper kieHelper = new KieHelper();
        kieHelper.addContent(ruleStr, ResourceType.DRL);
        //规则引擎回话
        KieSession kieSession = kieHelper.build().newKieSession();

        //时间输入的数据（facts）
        Event event = new Event("view", 2, false);
        //插入到规则引擎中（内存中）
        kieSession.insert(event);
        //应用所有的规则（才会指定drl文件中的内容）
        kieSession.fireAllRules();

        System.out.println("返回的结果：" + event.isHit());



    }
}
