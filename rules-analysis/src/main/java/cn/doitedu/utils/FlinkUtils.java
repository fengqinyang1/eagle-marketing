package cn.doitedu.utils;

import org.apache.flink.api.common.serialization.DeserializationSchema;
import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.runtime.state.filesystem.FsStateBackend;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.DataStreamSource;
import org.apache.flink.streaming.api.environment.CheckpointConfig;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.connectors.kafka.FlinkKafkaConsumer;
import org.apache.flink.streaming.connectors.kafka.KafkaDeserializationSchema;

import java.util.Arrays;
import java.util.List;
import java.util.Properties;

public class FlinkUtils {

    public static final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();

    public static ParameterTool parameterTool;

    public static <T> DataStream<T> createKafkaStream(String path, Class<? extends DeserializationSchema<T>> clazz) throws Exception {
        //parameterTool在Client端
        //parameterTool中有Hbase和ClickHouse相关的参数
        ParameterTool parameterTool = ParameterTool.fromPropertiesFile(path);
        String topics = parameterTool.get("kafka.input.topics");
        return createKafkaStream(parameterTool, topics, clazz);
    }


    public static <T> DataStream<T> createKafkaStream(ParameterTool parameterTool, String topicNames, Class<? extends DeserializationSchema<T>> clazz) throws Exception {

        String bootstrapServers = parameterTool.getRequired("bootstrap.servers");

        //将客户端的参数设置全局参数（Client将参数发送给jobManager，然后jobManager再将参数广播发送给属于该Job的所有TaskManager）
        env.getConfig().setGlobalJobParameters(parameterTool);

        //为了容错，要开启checkpoint
        env.enableCheckpointing(parameterTool.getLong("checkpoint.interval", 60000));
        //在checkpoint时，将状态保存到HDFS存储后端
        //env.setStateBackend(new EmbeddedRocksDBStateBackend());
        //env.getCheckpointConfig().setCheckpointStorage("file:///checkpoint-dir");
        //将job cancel后，保留外部存储的checkpoint数据（为了以后重新提交job恢复数据）
        env.getCheckpointConfig().enableExternalizedCheckpoints(CheckpointConfig.ExternalizedCheckpointCleanup.RETAIN_ON_CANCELLATION);
        Properties properties = new Properties();
        properties.setProperty("bootstrap.servers", parameterTool.getRequired("bootstrap.servers"));
        properties.setProperty("auto.offset.reset", parameterTool.get("auto.offset.reset", "earliest"));

        //String topics = parameterTool.getRequired("input.topics");
        List<String> topicList = Arrays.asList(topicNames.split(","));

        FlinkKafkaConsumer<T> kafkaConsumer = new FlinkKafkaConsumer<>(
                topicList,
                clazz.newInstance(),
                properties
        );

        //在checkpoint成功后，不将偏移量写入到Kafka特殊的topic中
        kafkaConsumer.setCommitOffsetsOnCheckpoints(false);

        DataStreamSource<T> lines = env.addSource(kafkaConsumer);

        return lines;
    }


    public static <T> DataStream<T> createKafkaStreamWithId(String path, Class<? extends KafkaDeserializationSchema<T>> clazz) throws Exception {

        parameterTool = ParameterTool.fromPropertiesFile(path);

        String bootstrapServers = parameterTool.getRequired("bootstrap.servers");

        //为了容错，要开启checkpoint
        env.enableCheckpointing(parameterTool.getLong("checkpoint.interval", 60000));
        //在checkpoint时，将状态保存到HDFS存储后端
        env.setStateBackend(new FsStateBackend(parameterTool.getRequired("checkpoint.path")));
        //将job cancel后，保留外部存储的checkpoint数据（为了以后重新提交job恢复数据）
        env.getCheckpointConfig().enableExternalizedCheckpoints(CheckpointConfig.ExternalizedCheckpointCleanup.RETAIN_ON_CANCELLATION);
        Properties properties = new Properties();
        properties.setProperty("bootstrap.servers", parameterTool.getRequired("bootstrap.servers"));
        properties.setProperty("auto.offset.reset", parameterTool.get("auto.offset.reset", "earliest"));

        String topics = parameterTool.getRequired("kafka.input.topics");
        List<String> topicList = Arrays.asList(topics.split(","));

        FlinkKafkaConsumer<T> kafkaConsumer = new FlinkKafkaConsumer<>(
                topicList,
                clazz.newInstance(),
                properties
        );

        //在checkpoint成功后，不将偏移量写入到Kafka特殊的topic中
        kafkaConsumer.setCommitOffsetsOnCheckpoints(false);

        DataStreamSource<T> lines = env.addSource(kafkaConsumer);

        return lines;
    }
}
