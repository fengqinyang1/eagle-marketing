package cn.doitedu.utils;

import cn.doitedu.pojo.CombineCondition;
import cn.doitedu.pojo.EventCondition;
import cn.doitedu.pojo.EventParam;
import cn.doitedu.pojo.RuleCondition;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Properties;

public class RuleSimulator {


    public static RuleCondition getRule() {

        //获取所有的规则（以后可以从广播状态中获取）
        //运营人员事先配置好的营销规则（参数名和参数值）

        RuleCondition ruleCondition = new RuleCondition();
        ruleCondition.setId(100000L);

        //事先定义好的触发行为规则
        String eventId = "A";
        HashMap<String, String> properties = new HashMap<>();
        properties.put("p2", "v1");
        properties.put("p20", "v2");
        EventCondition eventCondition = new EventCondition(eventId, properties);
        ruleCondition.setTriggerEvent(eventCondition);

        //事先定义好的用户画像规则
        HashMap<String, String> profileCondition = new HashMap<>();
        profileCondition.put("tag2", "v1");
        profileCondition.put("tag20", "v2");
        ruleCondition.setProfileCondition(profileCondition);

        //事先定义好的历史行为规则
        ArrayList<CombineCondition> combineConditions = new ArrayList<>();

        //查询历史行为次数的规则
        //规则1（历史行为次数）B事件在指定的时间范围内作为2或两次以上
        CombineCondition combineCondition1 = new CombineCondition();
        ArrayList<EventCondition> eventConditions1 = new ArrayList<>();
        EventCondition eventCondition1 = new EventCondition();
        eventCondition1.setEventId("E");
        eventConditions1.add(eventCondition1);
        combineCondition1.setEventConditions(eventConditions1);
        combineCondition1.setSql("select eventId from eagle_detail where deviceId = ? and timeStamp > ? and timeStamp <= ? order by timeStamp asc");
        combineCondition1.setStartTime(1636387200000L);
        combineCondition1.setEndTime(1637078399000L);
        combineCondition1.setRegex("(1)");
        combineCondition1.setMinLimit(2);
        combineCondition1.setMaxLimit(Integer.MAX_VALUE);

        //查询历史行为序列的规则
        //规则2（历史行为序列次数）ABD事件在指定的时间范围内出现过一次或一次以上
        CombineCondition combineCondition2 = new CombineCondition();
        ArrayList<EventCondition> eventConditions2 = new ArrayList<>();
        EventCondition eventCondition2 = new EventCondition();
        //行为序列：ABD  实际的eventId可能会是很长的字符串:productView, productAddCart
        eventCondition2.setEventId("E");
        EventCondition eventCondition3 = new EventCondition();
        eventCondition3.setEventId("H");
        EventCondition eventCondition4 = new EventCondition();
        eventCondition4.setEventId("I");
        eventConditions2.addAll(Arrays.asList(eventCondition2, eventCondition3, eventCondition4));
        combineCondition2.setSql("select eventId from eagle_detail where deviceId = ? and timeStamp > ? and timeStamp <= ? order by timeStamp asc");
        combineCondition2.setStartTime(1636387200000L);
        combineCondition2.setEndTime(1637078399000L);
        combineCondition2.setRegex("(.*?1.*?2.*?3)");
        combineCondition2.setMinLimit(1);
        combineCondition2.setMaxLimit(Integer.MAX_VALUE);
        combineCondition2.setEventConditions(eventConditions2);

        combineConditions.add(combineCondition1);
        combineConditions.add(combineCondition2);
        ruleCondition.setCombineConditions(combineConditions);


        //模拟一个时间触发的规则
        ruleCondition.setHasTimer(true);


        return ruleCondition;
    }
}
