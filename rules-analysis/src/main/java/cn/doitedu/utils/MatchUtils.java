package cn.doitedu.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MatchUtils {


    public static int matchCount(String sequenceStr, String regex) {

        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(sequenceStr);
        int count = 0;
        while (matcher.find()) {
            count++;
        }
        return count;
    }
}
