package cn.doitedu.utils;

import org.apache.commons.lang3.time.DateUtils;

import java.util.Calendar;
import java.util.Date;

public class TimeBoundUtil {

    //向上取整，减去两个小时
    public static long getTimeBound(long currentTime) {
        Date dt = DateUtils.ceiling(new Date(currentTime), Calendar.HOUR);
        return dt.getTime() - 2 * 3600 * 1000;
    }
}
