package cn.doitedu.utils;

import cn.doitedu.pojo.EventCondition;
import cn.doitedu.pojo.EventParam;
import cn.doitedu.pojo.LogBean;
import cn.doitedu.pojo.RuleCondition;

import java.util.Map;

/**
 * 用户触发事件的比较器
 */
public class EventComparator {


    public static boolean isMatchTriggerEvent(LogBean bean, EventCondition triggerEvent) {
        //当前用户输入的事件
        String currentEventId = bean.getEventId();
        //当前事件对应的行为数据
        Map<String, String> currentProps = bean.getProperties();
        //判断当前的事件ID和事先配置好的事件ID是否相同
        if (currentEventId.equals(triggerEvent.getEventId())) {
            //定义的条件对应的属性和规则
            Map<String, String> conditionProps = triggerEvent.getProperties();
            //循环事先定义的多个属性
            for (String key : conditionProps.keySet()) {
                //当前的一个行为属性值
                String value = currentProps.get(key);
                //事先配置好的该属性对应的值
                String conditionValue = conditionProps.get(key);
                if (!conditionValue.equals(value)) {
                    return false;
                }
            }
        } else {
            return false;
        }
        return true;
    }
}
